import { length } from 'stringz';

export const CharacterCounter: React.FC<{
  text: string;
  max: number;
}> = ({ text }) => {
  // NOTE: (toot.cat) shortcut our hack for counting up
  return <span className='character-counter'>{length(text)}</span>;
};
